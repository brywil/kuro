/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: menuTree.c
 *
 * About:
 *
 * This file defines the tree structure used by Onyxx-Commander.  The structure is formatted via several struct
 * elements.
 */

/*
 * To install vcan:
 *
 * < modprobe can
 * < modprobe can_raw
 * < modprobe vcan
 * < sudo ip link add dev vcan0 type vcan
 * < sudo ip link set up vcan0
 * < ip link show vcan0
 * > vcan0: <NOARP,UP,LOWER_UP> mtu 16 qdisc noqueue state UNKNOWN link/can
 */

#include "common.h"
#include "strChange.h"
#include "menuTree.h"
#include "cmdOptions.h"
#include "cmdConfig.h"
#include "cmdTransfer.h"

int var_chipid = 1; // Must be 1, not 0.
int var_relaxTimer = 40;
char var_candevice[255];
char var_firmwarefile[255];
char var_macaddr[9];
int var_devid;
int var_macint[MAC_LEN];

struct clinode_s menuTree[] = {
	/* Main Menu #0 */

	{ "Top Menu", 						0,   typTitle, 						{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 						{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 						{ .cb_var = 0 } },
	{ "CAN Device       [%s]", 		 	'A', typStrFunc | typString,		{ .cb_str = setCANDevice 	} },
	{ "Firmware File    [%s]", 			'B', typStrFunc | typString, 		{ .cb_str = setFirmwareFile } },
	{ "Device MAC       [%s]",			'C', typStrFunc | typString,	   	{ .cb_str = setMACAddr 		} },
	{ "Relax Timer (ms) [%d]",			'D', typInteger | typVar,        	{ .cb_var = &var_relaxTimer } },
	{ "",								0,   typEmpty,						{ .cb_var = 0 				} },
	{ "Program Firmware",				'1', typItemChoice | typCmdFunc, 	{ .cb_cmd = cmdFirmware 	} },
	{ "Program MAC",					'2', typItemChoice | typCmdFunc, 	{ .cb_cmd = cmdProgramMAC 	} },
	{ "Ping for CAN Devices",			'3', typItemChoice | typCmdFunc,    { .cb_cmd = cmdShowMAC		} },
	{ "Display PC CAN Devices",		 	'4', typItemChoice | typCmdFunc, 	{ .cb_cmd = cmdShowDevices 	} },
	{ "Get Running App ID",				'5', typItemChoice | typCmdFunc,    { .cb_cmd = cmdGetAppID 	} },
	{ "",								0, 	 typEmpty,						{ .cb_var = 0				} },
	{ "Get Device Configuration",		'G', typItemChoice | typCmdFunc,    { .cb_cmd = cmdGetConfInfo 	} },
	{ "Set Device Configuration",		'S', typItemChoice | typCmdFunc,    { .cb_cmd = cmdSetConfInfo 	} },
	{ "About",							'?', typItemChoice | typCmdFunc, 	{ .cb_cmd = cmdProductInfo 	} },
	{ "",								0,   typEmpty, 		{ 0 } },
	{ 0,								0, 	 typLast, 		{ 0 } },

	/* Last Entry */
	{ 0,								0, 	 typLast, 		{ 0 } },
};

