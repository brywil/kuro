/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: exCmd.c
 *
 * About:
 *
 * This file contains miscellaneous support functions for Onyxx-Commander files.
 */

#include "common.h"
#include "exCmd.h"
#include "menuTree.h"

char resultBuf[SIZEOF_RESULT];

extern int global_argmode;
extern int quietMode;

int executeCommand(char *cmdstr, char *outbuf)
{
  int retVal = CMD_ERROR; // default failure
  FILE *fp;
  char path[1035];

  /* Open the command for reading. */

  outbuf[0] = 0;
  fp = popen(cmdstr, "r");
  if (fp != NULL) {
	  /* Read the output a line at a time - output it. */

	  outbuf[0] = 0;

	  while (fgets(path, sizeof(path)-1, fp) != NULL)
	  {
		  strcat(outbuf, path);
		  strcat(outbuf, "\n");
	  }

  /* close */
	  pclose(fp);
	  retVal = CMD_SUCCESS;
  }

  return retVal;
}


/******************************************************
 * Function	   : strwbstr
 * Input	   : Haystack buffer pointer
 *               Stop char
 * 			     Ptr to starting point
 *               Ptr to string to find (needle)
 * Ouput	   : NULL if not found, Ptr of find str otherwise
 * Description : Similar to strstr(), however this function
 *               searches backwards, giving up when we reach
 *               the stop char.  Case sensitive.
 ******************************************************/
char *strbstr(char *haystack, char stopchar, char *starting, char *needle)
{
	char *p = starting;
	char *retVal = NULL;

	while (p && *p != stopchar && *p != 0 && p != haystack)
	{
		if (strncmp(p, needle, strlen(needle)) == 0)
		{
			retVal = p;
			break;
		} else
			p--;
	}

	return retVal;
}

struct termios initial_settings;
struct termios new_settings;

extern int exitflag;

/******************************************************
 * Function	   : cliSetup
 * Input	   : None
 * Ouput	   : None
 * Description : Initializes the terminal to be:
 * 				  non block, echo, no signal ints.
 ******************************************************/
void cliSetup()
{
	memset(&initial_settings, STDIN_FILENO, sizeof(struct termios));
	memset(&new_settings, STDIN_FILENO, sizeof(struct termios));

	tcgetattr(STDIN_FILENO, &initial_settings);
	new_settings = initial_settings;
	new_settings.c_lflag &= ~(ICANON | ECHO | ISIG);
	new_settings.c_cc[VMIN] = 0;
	new_settings.c_cc[VTIME] = 0;

	tcsetattr(0, TCSANOW, &new_settings);
}

/******************************************************
 * Function	   : cliRestore
 * Input	   : None
 * Ouput	   : None
 * Description : Restore terminal settings.  Requires a first
 *  			 call to cliSetup.
 ******************************************************/
void cliRestore()
{
	tcsetattr(STDIN_FILENO, TCSANOW, &initial_settings);
}

/******************************************************
 * Function	   : cliPrint
 * Input	   : string
 * Ouput	   : None
 * Description : Outputs given string to ANSI parser.  String
 *				 does not have to be ANSI, however.
 ******************************************************/
void cliPrint(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	if (quietMode == 0)
		vfprintf(stdout, fmt, ap); fflush(stdout);

	va_end(ap);
}

/******************************************************
 * Function	   : getInput
 * Input	   : Pointer to key collection buffer
 *               Length of buffer
 *               Char * to override value when in argument mode
 * Ouput	   : Number of bytes read
 * Description : Gets keyboard input from console.
 ******************************************************/
int getInput(char *keybuf, int len, char *override_value)
{
	int retVal = 0;
	char c;

	fflush(stdin);
	keybuf[0] = 0;

	cliSetup();

	memset(keybuf, 0, len);

	if (global_argmode && override_value)
	{
		if (strlen(override_value) < len)
		{
			strcpy(keybuf, override_value);
			retVal = strlen(keybuf);
			cliPrint(keybuf); // Make it look like it was manually entered.
			cliPrint("\n");
		}
	} else {
		while (1)
		{
			while ((c = getchar()) == EOF);

			if (c == KYB_ESCAPE) // ESCape erases all and aborts
			{
				retVal = 0;
				keybuf[0] = 0;
				break;
			} else if (c == KYB_ENTER) {
				break;
			} else if (c == '@' && exitflag == 0) {
				cliRestore();
				exit(0);
			} else if (c == '\b') {
				if (retVal != 0)
				{
					keybuf[retVal--] = 0;
					cliPrint("\b \b");
				}
			} else {
				if (retVal <= (len - 1))	{
					if (c >= ' ' && c <= '~')
					{
						printf("%c", c);
						keybuf[retVal++] = c;
						keybuf[retVal] = 0;
					}
				} else {
					cliPrint("\b \b"); // Never accept last key, needs to be NULL
					retVal--;
				}
			}
		}
	}

	cliRestore();
	cliPrint("\n");
	return retVal;
}

/******************************************************
 * Function		: strnicmp
 * Input		: char * compare string 1
 *                char * compare string 2
 * Output		: < 0, 0, >0 comparison (0 = equals)
 *
 * Description	: Compares wp strings up to n chars.
 ******************************************************/
int strnicmp (const char *s1, const char *s2, int len)
{
	char c1, c2;
	int v;

	if (len == 0)
		return 0;

	do {
		c1 = *s1++;
		c2 = *s2++;
		/* the casts are necessary when pStr1 is shorter & char is signed */
		v = (unsigned int) toupper(c1) - (unsigned int) toupper(c2);
	} while ((v == 0) && (c1 != '\0') && (--len > 0));

	return v;
}

int stricmp (const char *s1, const char *s2)
{
  while (*s2 != 0 && toupper(*s1) == toupper(*s2))
    s1++, s2++;

  return (int) (toupper(*s1) - toupper(*s2));
}
