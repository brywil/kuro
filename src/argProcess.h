/*
 * argProcess.h
 *
 *  Created on: Mar 20, 2017
 *      Author: bwilcutt
 */

#ifndef SRC_ARGPROCESS_H_
#define SRC_ARGPROCESS_H_

extern int processArguments(int argc, char *argv[]);

#endif /* SRC_ARGPROCESS_H_ */
