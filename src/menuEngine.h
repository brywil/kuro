/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: menuEngine.c
 *
 * About:
 *
 * Command include file.
 */

#ifndef MENUENGINE_H_
#define MENUENGINE_H_

#include "common.h"
#include "cliprop.h"

extern bool kbhit();
extern void displayMenu(struct cliProp_struct *wedProps);
extern void displayStandardMenu(struct cliProp_struct *wedProps);
extern void displayPrompt(struct cliProp_struct *wedProps);
extern int selectMenuOption(struct cliProp_struct *wedProps);
extern int parseCommand(struct cliProp_struct *wedProps, char c);
extern void standardMenuSelectorEnter(struct cliProp_struct *wedProps, char key, char *override_value);
extern void menuStack(struct cliProp_struct *wedProps);
extern int indexToMenu(struct cliProp_struct *wedProps);
extern char *parseSelect(int index);

#endif /* MENUENGINE_H_ */
