/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: menuTree.h
 *
 * About:
 *
 * Definitions and support data for all Onyxx-Commander files.
 */

#ifndef MENUTREE_H_
#define MENUTREE_H_

#include "common.h"

#define EXIT_ALL				-1
#define LYNX_MENU				1 // Last menu, does not exist

extern int var_devid;
extern int var_chipid;
extern int var_subprogram;
extern char var_candevice[255];
extern char var_firmwarefile[255];
extern char var_macaddr[9];
extern int var_relaxTimer;
extern int var_macint[MAC_LEN];

#endif /* MENUTREE_H_ */
