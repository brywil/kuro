/*
 * argProcess.c
 *
 *  Created on: Mar 20, 2017
 *      Author: bwilcutt
 */
#include "common.h"
#include "menuEngine.h"
#include "argProcess.h"

static int processRoute(char *route, char *value);
int quietMode = 0;

int processArguments(int argc, char *argv[])
{
	int retVal = 0;
	int i;
	int slen = 0;
	char argument[255];
	char value[255];
	char route[20];
	char *s;

	// Arguments are processed as if they were entered manually on the menu system by the user.
	// For example: -1dc:"hello" would mean option #1 from the main menu, #d from the submenu,
	// #c from the subsubmenu should be set tp the value of "hello".
	//
	// Command line options have the format of:
	// -{route string}:{value}
	// Where route string is the individual menu options to menus and submenus,
	// {value} indicates a value for an item.

	for (i = 1; i < argc && retVal == 0; i++)
	{
		strcpy(argument, argv[i]);
		memset(value, 0, sizeof(value));
		memset(route, 0, sizeof(route));

		// Copy the route from the path.  Route goes up to the first ":".

		s = strchr(&argument[1], ':');
		if (s == NULL)
		{
			s = &argument[strlen(argument)]; // No value
		}

		slen = ((s - 1) - argument);

		if (slen <= 0)
		{
			// A '-' turns display off and a '+' turns it back on.

			if (argument[0] == '-')
				quietMode = 1;
			else if (argument[0] == '+')
				quietMode = 0;
			else
				retVal = 1; // Error
		} else {
			// Routes cannot be longer than 19 bytes.

			if (slen < 20)
			{
				strncpy(route, &argv[i][1], slen);

				// Copy the value

				strcpy(value, s + 1);
			} else {
				retVal = 1;
			}

			if (!retVal)
			{
				// Process the route and argument

				retVal = processRoute(route, value);
			}
		}
	}

	return retVal;
}

static int processRoute(char *route, char *value)
{
	int retVal = 0;
	char c;
	int i;
	struct cliProp_struct view;

	// Set up stacking of the menu system.

	view.cliProp_prompt = "Error Mode (@-Exit): ";
	view.currentMenu = 0;

	if (route && value)
	{
		// Reset menu to main.

		view.currentMenu = 0;

		for (i = 0; i < strlen(route) && retVal == 0; i++)
		{
			// All but the last option requires the value parameter.

			c = route[i];

			if (route[i + 1] == 0)
			{
				// Last option, run it here.

				standardMenuSelectorEnter(&view, toupper(c), value);

			} else {
				retVal = parseCommand(&view, toupper(c));
			}
		}
	}

	return retVal;
}
