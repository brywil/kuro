/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: cmdOptions.c
 *
 * About:
 *
 * This file contains support functions for executing numerous menu options and
 * is used by the menu engine as support for the menu tree structure.
 */

#include <sys/time.h>
#include "common.h"
#include "cmdOptions.h"
#include "menuTree.h"
#include "onyxx_can_frame.h"
#include "menuEngine.h"
#include "appUtils.h"

int getResponses();
void getMacAddr(uint8_t *);
long getFileSize(char *fname);

/******************************************************
 * Function	   : cmdShowMAC
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Sends a request to all CAN devices to
 *				 report their MAC addresses.  Displays
 *				 the addresses as they are found.
 ******************************************************/
int cmdShowMAC(void)
{
	int retVal = CMD_SUCCESS;
	struct can_frame frame;
	lpp_id lpp;

	cliPrint("\nConnecting...");

	/* Send our packet */

	cliPrint("\nPing for MAC Addresses on CANBus, Please wait...\n");

	lpp.val = 0;
	lpp.bits.dtyp = TYPE_COMMAND;
	lpp.bits.mac0 = 0x00;
	lpp.bits.mac1 = 0x00;
	lpp.bits.mac2 = MLSM_IDENTIFICATION;
	lpp.bits.populate = HAS_ONE;
	lpp.bits.priority = 0;
	frame.can_id = lpp.val;
	frame.can_dlc = 2;
	frame.data[0] = 0;
	frame.data[1] = CMD_GET_MAC_REQ;

	CAN_Transmit(&frame);

		/* Wait for responses here */

	if ((retVal = getResponses()) == CMD_ERROR)
	{
		cliPrint("\nMAC Programming failed for one or more modules.  Verify & Retry.\n");
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdGetAppID
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Sends a GET APP ID command to the far end
 *               device and pritns any responses.  The purpose
 *               of this function is to check if the device
 *               has a working app.  The bootloader of the
 *               device does NOT respond to this command.
 ******************************************************/
int cmdGetAppID(void)
{
	int retVal = CMD_SUCCESS;
	struct can_frame frame;
	lpp_id lpp;

	cliPrint("\nConnecting...");

	/* Send our packet */

	cliPrint("\nPing for Application IDs on CANBus, Please wait...\n");

	lpp.val = 0;
	lpp.bits.dtyp = TYPE_COMMAND;
	lpp.bits.mac0 = 0x00;
	lpp.bits.mac1 = 0x00;
	lpp.bits.mac2 = MLSM_IDENTIFICATION;
	lpp.bits.populate = HAS_ONE;
	lpp.bits.priority = 0;
	frame.can_id = lpp.val;
	frame.can_dlc = 2;
	frame.data[0] = 0;
	frame.data[1] = CMD_APP_ID_REQ;

	CAN_Transmit(&frame);

		/* Wait for responses here */

	retVal = getResponses();

	return retVal;
}

/******************************************************
 * Function	   : cmdProgramMAC
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Sends the MAC across the CAN bus which causes
 *               all recipients to program themselves to that
 *               value.  CAREFUL use of this function must
 *               be observed.  Only a single device should
 *               be programmed at a time, do not use this
 *               function on an open CAN bus that has
 *               multiple devices.
 ******************************************************/
int cmdProgramMAC(void)
{
	int retVal = CMD_SUCCESS;
	uint8_t macint[3];
	struct can_frame frame;
	lpp_id lpp;

	if (strlen(var_macaddr) == 8)
	{
		getMacAddr(macint);

		/* Send our packet */

		cliPrint("\nProgramming, please wait...");

		lpp.val = 0;
		lpp.bits.dtyp = TYPE_COMMAND;
		lpp.bits.mac0 = macint[0];
		lpp.bits.mac1 = macint[1];
		lpp.bits.mac2 = macint[2];
		lpp.bits.populate = HAS_ONE;
		lpp.bits.priority = 0;
		frame.can_id = lpp.val;
		frame.can_dlc = 5; // Id + command + respond MAC
		frame.data[0] = 0; // All devices on MAC
		frame.data[1] = CMD_PGM_MAC_IND;
		frame.data[2] = var_macint[0];
		frame.data[3] = var_macint[1];
		frame.data[4] = var_macint[2];
		CAN_Transmit(&frame);
		sleep(1);

		cliPrint("\nCompleted.\n");
	} else {
		cliPrint("\nMAC has not been specified.  Aborting.\n");
	}

	return retVal;
}

/******************************************************
 * Function	   : GetMACResponses
 * Input       : None
 * Output      : CMD_ERROR | CMD_SUCCESS
 * Description : Waits for responses back from a MAC
 *               programming event.  Returns SUCCESS
 *               if a response is given by all
 *               subcomponents of a0, d0, ui, led
 *               and main.  Returns ERROR is timeout
 *               occurs without responses from all.
 ******************************************************/
int getResponses()
{
	int retVal = CMD_SUCCESS;
	struct can_frame cmdpkt;
	lpp_id lpp;
	struct timeval tv_start;
	struct timeval tv_current;
	struct timezone tz;

	gettimeofday(&tv_start, &tz);

	do {
		gettimeofday(&tv_current, &tz);

		if (readCANFrame(&cmdpkt) == CMD_SUCCESS)
		{
			lpp.val = cmdpkt.can_id;

			// We received a packet, decode.

			if (lpp.bits.dtyp == TYPE_COMMAND && (cmdpkt.data[1] == CMD_GET_MAC_REP || cmdpkt.data[1] == CMD_APP_ID_REP))
			{
				if (cmdpkt.data[1] == CMD_GET_MAC_REP && cmdpkt.can_dlc == 5)
				{
					cliPrint("Mac ID %02x:%02x:%02x  - ", cmdpkt.data[2], cmdpkt.data[3], cmdpkt.data[4]);
				} else if (cmdpkt.data[1] == CMD_APP_ID_REP && cmdpkt.can_dlc == 8) {
					cliPrint("APP ID %02x.%02x.%02x; %02x.%02x.%02x  - ", cmdpkt.data[2], cmdpkt.data[3], cmdpkt.data[4],
							  cmdpkt.data[5], cmdpkt.data[6], cmdpkt.data[7]);
				}

				switch(cmdpkt.data[0])
				{
					case TYPE_D0:
						cliPrint("d0\n");
						break;

					case TYPE_A0:
						cliPrint("a0\n");
						break;

					case TYPE_UI_L:
						cliPrint("ui lo\n");
						break;

					case TYPE_UI_H:
						cliPrint("ui hi\n");
						break;

					case TYPE_LED:
						cliPrint("led\n");
						break;

					case TYPE_LSM:
						cliPrint("lsm\n");
						break;

					case TYPE_MLSM:
						cliPrint("mlsm\n");
						break;

					default:
						cliPrint("unknown (%d)\n", cmdpkt.data[0]);
						break;
				}
			}
		}
	} while(tv_current.tv_sec < (tv_start.tv_sec + 1));

	return retVal;
}

/******************************************************
 * Function	   : cmdProductInfo
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Loads local variables with product info
 *               to be seen on ABOUT menu.
 ******************************************************/
int cmdProductInfo(void)
{
	int retVal = CMD_SUCCESS;

	cliPrint("\n\nProduct Information:\n\n");
	cliPrint("Kuro: Onyxx Firmware Programmer\n");
	cliPrint("Version 1.0\n");
	cliPrint("\n");
	cliPrint("By Bryan Wilcutt\n");
	cliPrint("1.1.2017\n");
	cliPrint("\n");
	cliPrint("Command Line Usage:\n");
	cliPrint("   >Kuro -{menu options}:{value}\n");
	cliPrint("  Where {menu options} = Option keys to each menu & submenu.\n");
	cliPrint("  Where {value} = Optional value to be given to last option key.\n");
	cliPrint("  A - with no option turns off display for any following commands.\n");
	cliPrint("  A + turns display back on for any following commands.\n");
	cliPrint(" Example: Kuro - -a:can0 -b:myfile.hex + -c:11:22:33\n -5\n");

	return retVal;
}

/******************************************************
 * Function	   : cmdShowDevices
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Displays the contents of the /dev
 * directory to the terminal.
 ******************************************************/
int cmdShowDevices(void)
{
	int retVal = CMD_ERROR;
	char outcmd[100];

	cliPrint("\nShowing Device Directory\n\n");
	sprintf(outcmd, "ip link show | grep can");
	retVal = system(outcmd);

	return retVal;
}

