/*
 * appUtils.c
 *
 *  Created on: Feb 1, 2017
 *      Author: bwilcutt
 */

#include "appUtils.h"
#include "common.h"
#include "cmdOptions.h"
#include "menuTree.h"
#include "onyxx_can_frame.h"
#include "menuEngine.h"

static int canSocket = -1;
static struct sockaddr_can canAddr;

int readCANFrame(struct can_frame *frame);


void CAN_Transmit(struct can_frame *frame)
{
	if (frame)
	{
		frame->can_id |= 0x80000000; // EFF to 1
		send(canSocket, frame, sizeof(struct can_frame), MSG_DONTWAIT);
	}
}
/******************************************************
 * Function	   : readCANFrame
 * Input	   : can_frame pointer of destination data
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Reads an extended CAN frame packet from
 *				 the given CAN socket.  The packet is
 *				 stored on the destination buffer.
 *				 Will return CMD_ERROR is no data was
 *				 read (a packet not available).
 ******************************************************/
int readCANFrame(struct can_frame *frame)
{
	int retVal = CMD_ERROR; // Failure
	fd_set rdfs;
	struct iovec iov;
	struct msghdr msg;
	char ctrlmsg[CMSG_SPACE(sizeof(struct timeval)) + CMSG_SPACE(sizeof(__u32))];
	int nbytes;
	struct timeval tv;

	/* These settings are static and can be held out of the hot path */

	iov.iov_base = frame;
	msg.msg_name = &canAddr;
	msg.msg_iov =  &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = &ctrlmsg;

	FD_ZERO(&rdfs);
	FD_SET(canSocket, &rdfs);

	tv.tv_sec = 3;
	tv.tv_usec = 0;

	if (select(canSocket + 1, &rdfs, NULL, NULL, &tv))
	{
		/* check socket */

		if (FD_ISSET(canSocket, &rdfs))
		{
			/* these settings may be modified by recvmsg() */

			iov.iov_len = sizeof(struct can_frame);
			msg.msg_namelen = sizeof(struct sockaddr_can);
			msg.msg_controllen = sizeof(ctrlmsg);
			msg.msg_flags = 0;

			nbytes = recvmsg(canSocket, &msg, 0);

			if (nbytes == sizeof(struct can_frame))
				retVal = CMD_SUCCESS; // success
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : openCAN
 * Input	   : None
 * Output	   : CMD_ERROR | CMD_SUCCESS
 * Description : Opens a socket on the specified CAN
 * 				 device.
 ******************************************************/
int openCAN()
{
	int retVal = CMD_ERROR;
	struct ifreq ifr;
	struct can_filter rfilter;

	canSocket = -1;

	memset(&ifr, 0x0, sizeof(ifr));
	memset((uint8_t *) &canAddr, 0x0, sizeof(struct sockaddr_can));

	if ((canSocket = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
	{
		cliPrint("CAN socket cannot be opened.\n");
	} else {

		/* Bind to the socket */

		strcpy(ifr.ifr_name, var_candevice);
		cliPrint("\nUsing device %s\n", ifr.ifr_name);

		if (ioctl(canSocket, SIOCGIFINDEX, &ifr) < 0) {
			cliPrint("\nerr: ioctl() - %s\n", strerror(errno));
			close(canSocket);
			canSocket = -1;
		} else {
			canAddr.can_ifindex = ifr.ifr_ifindex;
			canAddr.can_family = AF_CAN;

			/* Turn off filters */

			rfilter.can_id = 0; // All frames...
			rfilter.can_mask = 0; // ... will pass to this socket
			setsockopt(canSocket, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));

			if (bind(canSocket, (struct sockaddr *) &canAddr, sizeof(struct sockaddr_can)) < 0)
			{
				cliPrint("\nerr: bind() - %s\n", strerror(errno));
				close(canSocket);
				canSocket = -1;
			} else {
				retVal = CMD_SUCCESS;
			}
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : getMacAddr
 * Input	   : uint8_t * to 3-byte destination
 * Output	   : None
 * Description : Converts MAC string format into 3-byte
 *				 integer array.
 ******************************************************/
void getMacAddr(uint8_t *macint)
{
	unsigned int v1, v2, v3;

	sscanf(var_macaddr, "%02x:%02x:%02x", &v1, &v2, &v3);
	macint[0] = v1;
	macint[1] = v2;
	macint[2] = v3;
}

/******************************************************
 * Function	   : getFileSize
 * Input	   : char * to file name
 * Output	   : Size of file, also 0 if error
 * Description : Returns the size of the file indicated by
 *				 the file name.
 ******************************************************/
long getFileSize(char *fname)
{
	long retVal = 0;
	struct stat fstatdata;

	if (fname)
	{
		stat(var_firmwarefile, &fstatdata);
		retVal = fstatdata.st_size;
	}

	return retVal;
}

