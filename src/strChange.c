/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: strChange.c
 *
 * About:
 *
 * String management and changing functions for the menu engine are handled here.
 */

#include "common.h"
#include "strChange.h"
#include "menuTree.h"
#include "exCmd.h"
#include "appUtils.h"

static int setDeviceByFilename(char *);

/******************************************************
 * Function	   : setCANDevice
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of CAN Device string
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets CAN device string.
 ******************************************************/
int setCANDevice(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;

	if (writable)
	{
		// Verify the device exists.

		if (strlen(newval) > (sizeof(var_candevice) - 1))
		{
			retVal = CMD_ERROR;
			cliPrint("\nFile name too large, %s maximum.\n", sizeof(var_candevice));
		} else {
			strcpy(var_candevice, newval);

			if (openCAN() == CMD_ERROR)
			{
				var_candevice[0] = 0;
				cliPrint("\nCannot open CAN device.\n");
				retVal = CMD_ERROR;
			}
		}
	}

	if (retVal == CMD_SUCCESS)
	{
		sprintf(newval, "%s", var_candevice);
	}

	return retVal;
}

/******************************************************
 * Function	   : setFirmwareFile
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of File Name string
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets firmware file name string.
 ******************************************************/
int setFirmwareFile(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	struct stat fileStat;

	if (writable)
	{
		// Verify the device exists.

		if (stat(newval, &fileStat))
		{
			retVal = CMD_ERROR;
			cliPrint("\nInvalid filename, file not found.\n");
		} else {
			if (strlen(newval) > (sizeof(var_firmwarefile) - 1))
			{
				retVal = CMD_ERROR;
				cliPrint("\nFile name too large, %s maximum.\n", sizeof(var_firmwarefile));
			} else {
				if ((retVal = setDeviceByFilename(newval)) == CMD_SUCCESS)
				{
					strcpy(var_firmwarefile, newval);
				} else {
					var_devid = 0;
				}
			}
		}
	}

	if (retVal == CMD_SUCCESS)
	{
		sprintf(newval, "%s", var_firmwarefile);
	}

	return retVal;
}

/******************************************************
 * Function	   : setMACAddr
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of MAC string
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets MAC string.  MAC Addr
 * 				 string is in the format of
 * 				 xx:xx:xx, which represents the UID (last
 * 				 three bytes) of a MAC address.
 ******************************************************/
int setMACAddr(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	int a, b, c;

	if (writable)
	{
		if (sscanf(newval, "%02x:%02x:%02x", &a, &b, &c) == 3)
		{
			if ((a >= 0 && a <= 0xff) && (b >= 0 && b <= 0xff) && (c >= 0 && c <= 0xff))
			{
				retVal = CMD_SUCCESS;
				sprintf(var_macaddr, "%02x:%02x:%02x", a, b, c);
				var_macint[0] = a;
				var_macint[1] = b;
				var_macint[2] = c;
			} else {
				cliPrint("\nInvalid format. Must be XX:XX:XX where XX = Hex Value.\n");
				retVal = CMD_ERROR;
			}
		} else {
			cliPrint("\nInvalid format. Must be XX:XX:XX where XX = Hex Value.\n");
			retVal = CMD_ERROR;
		}
	}

	if (retVal == CMD_SUCCESS)
	{
		sprintf(newval, "%s", var_macaddr);
	}

	return retVal;
}

static int setDeviceByFilename(char *fname)
{
	int retVal = CMD_SUCCESS; // Default

	if (fname)
	{
		if (strstr(fname, "a0"))
			var_chipid = TYPE_A0;
		else if (strstr(fname, "d0"))
			var_chipid = TYPE_D0;
		else if (strstr(fname, "ui_l"))
			var_chipid = TYPE_UI_L;
		else if (strstr(fname, "ui_h"))
			var_chipid = TYPE_UI_H;
		else if (strstr(fname, "led"))
			var_chipid = TYPE_LED;
		else if (strstr(fname, "mlsm"))
			var_chipid = TYPE_MLSM;
		else if (strstr(fname, "lsm"))
			var_chipid = TYPE_LSM;
		else
			retVal = CMD_ERROR;
	}

	return retVal;
}
