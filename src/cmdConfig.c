/*
 * cmdConfig.c
 *
 *  Created on: Feb 20, 2017
 *      Author: bwilcutt
 */
#include <sys/time.h>
#include "common.h"
#include "cmdOptions.h"
#include "menuTree.h"
#include "onyxx_can_frame.h"
#include "onyxxCfg.h"
#include "menuEngine.h"
#include "appUtils.h"
#include "cmdConfig.h"

config_fsm_t sendFsm = fsm_idle;
config_fsm_t recvFsm = fsm_idle;

// Info regarding device we are exchanging data with.

lpp_id xchgDevice;
struct configData configInfo;

// Xchg information for buffering

uint8_t configState = 0;
uint8_t cbuf[sizeof(struct configData) + 6];
static int cmdGetConfigFSM(struct can_frame *frame);
static int cmdSetConfigFSM(struct can_frame *frame);

#define CONFIG_TIMEOUT 20

static int configTimerCount = 0;

/******************************************************
 * Function	   : cmdGetConfigInfo
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Requests config info from the device.  This
 * 				 only works on an LSM or MLSM.
 ******************************************************/
int cmdGetConfInfo(void)
{
	int retVal = CMD_ERROR;
	struct can_frame frame;
	lpp_id lpp;

	configTimerCount = 0; // Reset timer

	// We only send config requests to LSM or mLSMs.

	if (var_chipid == TYPE_LSM || var_chipid == TYPE_MLSM)
	{
		cliPrint("\n\nRequesting configuration structure from %s\n",
				(var_chipid == TYPE_LSM) ? "LSM" : "mLSM");

		// Setup variables to track the far end device we're speaking with.

		xchgDevice.val = 0;
		xchgDevice.bits.mac0 = var_macint[0];
		xchgDevice.bits.mac1 = var_macint[1];
		xchgDevice.bits.mac2 = var_macint[2];
		xchgDevice.bits.dtyp = TYPE_COMMAND;
		xchgDevice.bits.populate = HAS_ONE;
		xchgDevice.bits.priority = 0;

		// Send the CMD_GET_CONFIG_REQ message to the far end device.

		frame.can_id = xchgDevice.val;
		frame.can_dlc = 5;
		frame.data[0] = 0;
		frame.data[1] = CMD_GET_CONFIG_REQ;
		frame.data[2] = KURO_MAC_BYTE_0; // Tell receiver who we are so we can get the responses later
		frame.data[3] = KURO_MAC_BYTE_1;
		frame.data[4] = KURO_MAC_BYTE_2;

		CAN_Transmit(&frame);
		recvFsm = fsm_idle;
		sendFsm = fsm_idle;

		configState = RECV_CONFIG_DATA;

		// Prepare temp buffer to receive configuration information

		memset(cbuf, 0, sizeof(cbuf));

		retVal = CMD_SUCCESS;
	}

	// Linger here until we timeout or until the transfer has completed.

	while (recvFsm != fsm_stop)
	{
		// Read CAN Bus

		if (readCANFrame(&frame) != CMD_ERROR)
		{
			lpp.val = frame.can_id;

			if (lpp.bits.mac0 == KURO_MAC_BYTE_0 && lpp.bits.mac1 == KURO_MAC_BYTE_1 && lpp.bits.mac2 == KURO_MAC_BYTE_2)
			{
				if (cmdGetConfigFSM(&frame) == CMD_ERROR)
				{
					// Error occurred, abort.

					recvFsm = fsm_idle;
					break;
				} else {
					configTimerCount = 0;
				}
			}
		}

		if (configTimerCount++ > CONFIG_TIMEOUT)
		{
			cliPrint("\nTIMEOUT: Config download aborted.\n");
			retVal = CMD_ERROR;
			break;
		}
	}

	return retVal;
}


/******************************************************
 * Function	   : cmdGetConfigFSM
 * Input	   : struct can_frame pointer
 * Output	   : CMD_ERROR | CMD_SUCCESS
 * Description : Requests config info from the device.  This
 * 				 only works on an LSM or MLSM.  Returns
 * 				 CMD_ERROR if message is not consumed by
 * 				 our state machine.
 ******************************************************/
static int cmdGetConfigFSM(struct can_frame *frame)
{
	int retVal = CMD_SUCCESS; // Default OK
	static int bufIndex = 0;

	// Set up to send a packet back to sender.

	frame->can_id = xchgDevice.val;
	frame->data[0] = 0;

	// React to the frame type based on our current state machine.

	switch(recvFsm)
	{
		case fsm_idle: // receive first frame of config (ind)
			// First packet of a data exchange, handle here.
			// Put data into our temporary buffer space.

			bufIndex = 6;
			memcpy(&cbuf[0], (uint8_t *) &frame->data[2], 6);

			frame->can_dlc = 2; // ID + Command
			frame->data[1] = CMD_HEXDATA_RESP;

			CAN_Transmit(frame);
			recvFsm = fsm_wait_resp;
			break;

		case fsm_wait_resp: // After sending a HEXDATA_RESP, we require a IND to continue
			// React only to a RESP to us from our exchange device.

			if (frame->data[1] == CMD_HEXDATA_IND || frame->data[1] == CMD_HEXLAST_IND)
			{
				// Copy more config data to send

				memcpy(&cbuf[bufIndex], &frame->data[2], frame->can_dlc - 2);
				bufIndex += (frame->can_dlc - 2);

				if (frame->data[1] == CMD_HEXLAST_IND)
				{
					frame->data[1] = CMD_HEXLAST_RESP;
					frame->can_dlc = 2;

					memcpy((uint8_t *) &configInfo, cbuf, sizeof(struct configData));

					bufIndex = 0;
					recvFsm = fsm_stop;
				} else {
					// One of many packets to send

					frame->data[1] = CMD_HEXDATA_RESP;
					frame->can_dlc = 2;  // ID + Command
				}

				CAN_Transmit(frame);

			} else {
				cliPrint("Unexpected command (%i), aborting.\n", frame->data[1]);
				bufIndex = 0;
				retVal = CMD_ERROR;
			}

			break;

		default:
			retVal = CMD_ERROR;
			bufIndex = 0;
			break;
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdSetConfigInfo
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Requests to have config information sent
 * 				 from this device to an LSM or MLSM for writing.
 ******************************************************/
int cmdSetConfInfo(void)
{
	int retVal = CMD_ERROR;
	struct can_frame frame;
	lpp_id lpp;

	configTimerCount = 0; // Reset timer

	// We only send config requests to LSM or mLSMs.

	if (var_chipid == TYPE_LSM || var_chipid == TYPE_MLSM)
	{
		cliPrint("\n\nSetting configuration data to %s\n",
				(var_chipid == TYPE_LSM) ? "LSM" : "mLSM");

		// Setup variables to track the far end device we're speaking with.

		xchgDevice.val = 0;
		xchgDevice.bits.mac0 = var_macint[0];
		xchgDevice.bits.mac1 = var_macint[1];
		xchgDevice.bits.mac2 = var_macint[2];
		xchgDevice.bits.dtyp = TYPE_COMMAND;
		xchgDevice.bits.populate = HAS_ONE;
		xchgDevice.bits.priority = 0;

		// Send the CMD_GET_CONFIG_REQ message to the far end device.

		frame.can_id = xchgDevice.val;
		frame.can_dlc = 5;
		frame.data[0] = 0;
		frame.data[1] = CMD_SET_CONFIG_REQ;
		frame.data[2] = KURO_MAC_BYTE_0; // Tell receiver who we are so we can get the responses later
		frame.data[3] = KURO_MAC_BYTE_1;
		frame.data[4] = KURO_MAC_BYTE_2;

		CAN_Transmit(&frame);
		sendFsm = fsm_idle;
		recvFsm = fsm_idle;
		configState = RECV_CONFIG_DATA;

		// Put the data we'll move to the LSM/MLSM in a temp buffer

		memcpy((uint8_t *) &configInfo, cbuf, sizeof(configInfo));

		// If we're sending, start it off here.

		retVal = cmdSetConfigFSM(&frame);
	}

	// Linger here until we timeout or until the transfer has completed.

	while (sendFsm != fsm_stop && retVal == CMD_SUCCESS)
	{
		// Read CAN Bus

		if (readCANFrame(&frame) != CMD_ERROR)
		{
			lpp.val = frame.can_id;

			if (lpp.bits.mac0 == KURO_MAC_BYTE_0 && lpp.bits.mac1 == KURO_MAC_BYTE_1 && lpp.bits.mac2 == KURO_MAC_BYTE_2)
			{
				if (cmdSetConfigFSM(&frame) == CMD_ERROR)
				{
					// Error occurred, abort.

					sendFsm = fsm_idle;
					break;
				} else {
					configTimerCount = 0;
				}
			}
		}

		if (configTimerCount++ > CONFIG_TIMEOUT)
		{
			cliPrint("\nTIMEOUT: Config upload aborted.\n");
			retVal = CMD_ERROR;
			break;
		}
	}

	sendFsm = fsm_idle;
	return retVal;
}


/******************************************************
 * Function	   : cmdSetConfigFSM
 * Input	   : struct can_frame pointer
 * Output	   : CMD_ERROR | CMD_SUCCESS
 * Description : Sets config info to an LSM or MLSM device.
 ******************************************************/
static int cmdSetConfigFSM(struct can_frame *frame)
{
	int retVal = CMD_SUCCESS; // Default OK
	static int bufIndex = 0;

	// Set up to send a packet back to sender.

	frame->can_id = xchgDevice.val;
	frame->data[0] = 0;

	// React to the frame type based on our current state machine.

	switch(sendFsm)
	{
		case fsm_idle: // receive first frame of config (ind)
			// First packet of a data exchange, handle here.
			// Move data from temp buffer to frame.

			bufIndex = 6;
			memcpy((uint8_t *) &frame->data[2], &cbuf[0], 6);

			frame->can_dlc = 8; // ID + Command + Datasize
			frame->data[1] = CMD_HEXDATA_IND;

			CAN_Transmit(frame);
			sendFsm = fsm_wait_resp;
			break;

		case fsm_wait_resp: // After sending a HEXDATA_IND, we require a RESP to continue
			// React only to a RESP to us from our exchange device.

			if (frame->data[1] == CMD_HEXDATA_RESP)
			{
				// Copy more config data to send

				memcpy(&frame->data[2], &cbuf[bufIndex], 6);
				bufIndex += 6;

				if (bufIndex >= sizeof(struct configData))
				{
					frame->data[1] = CMD_HEXLAST_IND;
					frame->can_dlc = 2; // ID + Command
					frame->can_dlc += (6 - (bufIndex - sizeof(struct configData)));

					sendFsm = fsm_finish; // Look for the REPLY
				} else {
					// One of many packets to send

					frame->data[1] = CMD_HEXDATA_IND;
					frame->can_dlc = 8;  // ID + Command + Data
				}

				CAN_Transmit(frame);
			} else {

				cliPrint("Unexpected command (%i), aborting.\n", frame->data[1]);
				bufIndex = 0;
				retVal = CMD_ERROR;
			}

			break;

		case fsm_finish:

			// Our HEXLAST_RESP will trigger us to reply with a CMD_SET_CONFIG_REP which
			// resets our state machine.

			if (frame->data[1] == CMD_HEXLAST_RESP)
			{
				cliPrint("\nUpload succeed, remote device now re-programming.\n");

				bufIndex = 0;
				sendFsm = fsm_stop; // Restore state machine
			}

			break;

		default:
			bufIndex = 0;
			sendFsm = fsm_stop;
			retVal = CMD_ERROR;
			break;
	}

	return retVal;
}
