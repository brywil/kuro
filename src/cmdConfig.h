/*
 * cmdConfig.h
 *
 *  Created on: Feb 20, 2017
 *      Author: bwilcutt
 */

#ifndef SRC_CMDCONFIG_H_
#define SRC_CMDCONFIG_H_

#include "common.h"
#include "exCmd.h"

typedef enum {
    fsm_idle        = 0,
    fsm_wait_resp   = 1,
    fsm_finish      = 2,
	fsm_stop		= 3
} config_fsm_t;

#define SEND_CONFIG_DATA	1
#define RECV_CONFIG_DATA	2

#define MASK_MAC_ONLY 0x00ffffff

int cmdGetConfInfo(void);
int cmdSetConfInfo(void);

#endif /* SRC_CMDCONFIG_H_ */
