/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: strChange.h
 *
 * About:
 *
 * Support for string handler C functions.
 */

#ifndef STRCHANGE_H_
#define STRCHANGE_H_

extern int setDeviceID(unsigned char writable, char *newval);
extern int setCANDevice(unsigned char writable, char *newval);
extern int setFirmwareFile(unsigned char writable, char *newval);
extern int setMACAddr(unsigned char writable, char *newval);

#endif /* STRCHANGE_H_ */
