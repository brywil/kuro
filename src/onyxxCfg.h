#ifndef SRC_ONYXXCFG_H_
#define SRC_ONYXXCFG_H_

/* Configuration Information
 *
 * This is the structure used to transfer configuration information using the
 * CMD_GET_CONFIG_REQ and CMD_SET_CONFIG_REQ.
 */

#define CONFIG_PASSWORD_SIZE	16
#define CONFIG_NAME_SIZE	32

struct configData {
	struct {
		uint8_t addr_a; // a.b.c.d in binary
		uint8_t addr_b;
		uint8_t addr_c;
		uint8_t addr_d;
	} eth0;

	struct {
		uint8_t addr_a; // a.b.c.d in binary
		uint8_t addr_b;
		uint8_t addr_c;
		uint8_t addr_d;
	} eth1;

	struct {
		uint32_t baud;
	} rs485_0;

	struct {
		uint32_t baud;
	} rs485_1;

	struct {
		uint32_t baud;
		uint8_t databits;
		uint8_t parity;
		uint8_t stopbits;
	} rs232_0;

	struct {
		uint32_t baud;
		uint8_t databits;
		uint8_t parity;
		uint8_t stopbits;
	} rs232_1;

	uint8_t username[CONFIG_NAME_SIZE];
	uint8_t password[CONFIG_PASSWORD_SIZE];
};

#endif /* SRC_ONYXXCFG_H_ */
