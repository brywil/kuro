/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: menuEngine.c
 *
 * About:
 *
 * Main menu processing is handled herein.
 */

#include "common.h"
#include "menuEngine.h"
#include "cliprop.h"
#include "menuTree.h"
#include "cmdOptions.h"
#include "appUtils.h"
#include "argProcess.h"

extern struct clinode_s menuTree[];

struct {
	char commandLine[132];
    struct clinode_s *commandNode;
} commandBuffer[40];

int exitflag = 1;
int global_argmode = 0;

/******************************************************
 * Function	   : main
 * Input	   : int Argument count
 * 			     Pointer array to arguments
 * Output	   : None
 * Description : Starts the Onyxx-Commander system.
 ******************************************************/
int main(int argc, char *argv[])
{
	struct cliProp_struct view;

	var_candevice[0] = 0;
	var_firmwarefile[0] = 0;
	var_macaddr[0] = 0;

	view.cliProp_prompt = "Choice (@-Exit): ";
	view.currentMenu = 0;

	if (argc > 1)
	{
		global_argmode = 1;

		processArguments(argc, argv);
	}

	if (global_argmode == 0)
	{
		// Start menu system

		cliPrint("\n\n************************************\n");
		cliPrint("*    KURO - THE PIC APP FLASHER    *\n");
		cliPrint("*                                  *\n");
		cliPrint("* Copyright - All Rights Reserved  *\n");
		cliPrint("*          LynxSpring, Inc.        *\n");
		cliPrint("************************************\n");
		menuStack(&view);
	}

	return 0;
}

/******************************************************
 * Function	   : menuStack
 * Input	   : Menu Properties ptr
 * Output	   : None
 * Description : Begins processing of the menu stack as
 *				 defined in the menu tree.
 ******************************************************/
void menuStack(struct cliProp_struct *wedProps)
{
	int exitMode = 0;

	do {
		displayMenu(wedProps);

		exitMode = selectMenuOption(wedProps);

		/* Allow up and down scroll if in ansi mode. */

	} while (exitMode == false);
}

/******************************************************
 * Function	   : selectMenuOption
 * Input	   : Properties
 * Output	   : True = exit mode select, False = no exit
 * Description : Gets input from user for ANSI or non-ANSI
 *				 modes.
 ******************************************************/
int selectMenuOption(struct cliProp_struct *wedProps)
{
	char c[2];
	int retVal = false;

	/* Get input */

	if (getInput(c, sizeof(c), NULL))
		parseCommand(wedProps, c[0]);

	return retVal;
}

/******************************************************
 * Function	   : parseCommand
 * Input	   : Properties
 *               Parse key from input
 * Output	   : True = exit selected, False = No Exit
 * Description : Parses commands for ANSI mode keyboard
 *				 input.
 ******************************************************/
int parseCommand(struct cliProp_struct *wedProps, char c)
{
	int retVal = 0;

	if (c == 0)
	{
		displayPrompt(wedProps);
	} else {
		if (c == '@')
			exit(0);

		standardMenuSelectorEnter(wedProps, (char) toupper(c), NULL);
	}

	return retVal;
}

/******************************************************
 * Function	   : indexToMenu
 * Input	   : Properties
 * Ouput	   : Index into menuTree for current menu
 * Description : Returns the index in the menuTree to
 *               the current menu.
 ******************************************************/
int indexToMenu(struct cliProp_struct *wedProps)
{
	int retVal = 0;
	int menuCount = 0;

	while (menuCount < wedProps->currentMenu)
	{
		if (menuTree[++retVal].cliFlags == typTitle)
			menuCount++;
	}

	return retVal;
}

/******************************************************
 * Function	   : standardMenuSelectorEnter
 * Input	   : Properties
 *               Char key of input
 *               Char pointer to override value when in argument mode
 * Ouput	   : None
 * Description : Operates on the select menu item as
 *				 specified by that menu item.
 ******************************************************/
void standardMenuSelectorEnter(struct cliProp_struct *wedProps, char key, char *override_value)
{
	int i = 0;
	int s = 0;
	int v1, v2, v3, v4;
	char newval[100];

	/* Search list to make sure key is legitimate for given menu */

	i = indexToMenu(wedProps);
	while (menuTree[i].cliFlags != typLast)
	{
		if (menuTree[i].cliKey == key && !(menuTree[i].cliFlags & typHidden))
			break;
		else {
			i++;
			s++;
		}
	}

	i = indexToMenu(wedProps) + s;

	if (menuTree[i].cliFlags != typLast)
	{
		switch ((menuTree[i].cliFlags & (typMenuChoice | typInteger | typSel | typString | typIpAddr | typCmdFunc )))
		{
			case typCmdFunc:
				if (menuTree[i].cb_cmd() == CMD_SUCCESS)
					cliPrint("\nCompleted\n");
				break;

			case typMenuChoice:
				wedProps->currentMenu = menuTree[i].nextMenu;
				break;

			case typSel:

				if (menuTree[i].cliFlags & typIntFunc)
				{
					menuTree[i].cb_int(true);
				}
				break;

			case typInteger:
				cliPrint("Enter new value (esc to abort): ");
				getInput(newval, sizeof(newval), override_value);

				if (menuTree[i].cliFlags & typInteger)
				{
					*((int *) menuTree[i].cb_var) = atoi(newval);
				}
				break;

			case typString:
				cliPrint("Enter new value (esc to abort): ");
				getInput(newval, sizeof(newval), override_value);

				if (menuTree[i].cliFlags & typStrFunc)
				{
					menuTree[i].cb_str(true, newval);
				}
				break;

			case typIpAddr:
				cliPrint("Enter IP address (xxx.xxx.xxx.xxx): ");
				getInput(newval, sizeof(newval), override_value);

				sscanf(newval, "%d.%d.%d.%d", &v1, &v2, &v3, &v4);
				if (v1 < 0 || v1 > 255)
					v1 = 0;
				if (v2 < 0 || v2 > 255)
					v2 = 0;
				if (v3 < 0 || v3 > 255)
					v3 = 0;
				if (v4 < 0 || v4 > 255)
					v4 = 0;

				if (menuTree[i].cliFlags & typStrFunc)
					menuTree[i].cb_str(true, newval);
				else
					sprintf((char *) menuTree[i].cb_var, "%d.%d.%d.%d", v1, v2, v3, v4);
				break;
			default:
				break;
		}
	}
}

/******************************************************
 * Function	   : displayPrompt
 * Input	   : Properties
 * Output	   : None
 * Description : Displays prompt in prompt window for
 *				 ANSI, or outputs it for non-ANSI.
 ******************************************************/
void displayPrompt(struct cliProp_struct *wedProps)
{
	cliPrint(wedProps->cliProp_prompt);
}


/******************************************************
 * Function	   : displayMenu
 * Input	   : wedProps
 *				 Command Array
 * Output	   : None
 * Description : Displays menu starting from current global
 *				 and including all immediate children.  Stops
 *				 When reaching next item of the same level as
 *				 the current menu item.
 ******************************************************/
void displayMenu(struct cliProp_struct *wedProps)
{
	displayStandardMenu(wedProps);
	displayPrompt(wedProps);
}

/******************************************************
 * Function	   : displayStandardMenu
 * Input	   : wedProps
 *				 Command Array
 * Output	   : None
 * Description : Displays menu starting from current global
 *				 and including all immediate children.  Displays
 *				 menu in the specific window (top or bottom) as
 *				 dictated by properties.
 ******************************************************/
void displayStandardMenu(struct cliProp_struct *wedProps)
{
	struct clinode_s *node = NULL;
	int index = indexToMenu(wedProps);

	// Clear the area

	cliPrint("\n\n\n");

	while (menuTree[index].cliFlags != typLast) {

		if (menuTree[index].cliFlags & typHidden)
		{
			index++;
			continue;
		}

		node = (struct clinode_s *) &menuTree[index];

		switch((node->cliFlags & (typTitle | typMenuChoice | typSel | typInteger | typString | typIpAddr | typItemChoice)))
		{
			case typTitle:
				cliPrint("%s\n", node->cliString);
				break;

			case typMenuChoice:
				// This is just an item that is unselectable but shows up in a menu.
				if (node->cliKey != 0)
					cliPrint("%c. %s\n", node->cliKey, node->cliString);
				else
					cliPrint("%s\n", node->cliString);

				break;

			case typSel:
			case typString:
			case typIpAddr:
			case typItemChoice:
			case typInteger:
				if (node->cliKey != 0)
					cliPrint("%c. %s\n", node->cliKey, parseSelect(index));
				else
					cliPrint("%s\n", parseSelect(index));

				break;

			default:
				if (node->cliKey != 0)
					cliPrint("%c. %s\n", node->cliKey, node->cliString);
				else
					cliPrint("%s\n", node->cliString);

				break;
		}

		index++;
	}
}

/******************************************************
 * Function	   : parseSelect
 * Input	   : menuTree index
 * Output	   : Char pointer to results
 * Description : Interprets the menuTree string into a
 *				 finished string.  Replaces "{" and "}"
 *				 with spaces for items not selected,
 *				 Selects "[" and "]" for item that is
 *				 selected.
 ******************************************************/
char *parseSelect(int index)
{
	int numItems = 0;
	int selItem = 0;
	char *s;
	int i, x;
	char newval[255];
	static char buffer[255];


	/* For call back functions, ask which item should be selected. */

	if (menuTree[index].cliFlags & typIntFunc)
	{
		/* How many index items are there?  Count by counting the number "{". */

		s = menuTree[index].cliString;

		for (i = 0; i < strlen(s); i++)
		{
			if (s[i] == '{')
				numItems++;
		}

		selItem = menuTree[index].cb_int(false); /* false = just reading, not writing! */
		if (selItem > numItems)
			selItem = 0;

		/* Go through the string, replace non selected item braces with spaces.  Selected items
		 * will have brackets replace their braces.
		 */

		strcpy(buffer, s);
		s = buffer;

		x = 0;

		while (s != NULL)
		{
			if ((s = strchr(s, '{')) != NULL)
			{
				x++;

				if (x == selItem)
				{
					*s = '[';

					/* Find closing brace */

					s = (char *) strchr(s, '}');
					*s = ']';
				} else {
					*s = ' ';

					/* Find closing brace */

					s = (char *) strchr(s, '}');
					*s = ' ';
				}
			}
		}
	} else if (menuTree[index].cliFlags & typStrFunc) {
		menuTree[index].cb_str(false, newval);

		sprintf(buffer, menuTree[index].cliString, newval);
	} else if ((menuTree[index].cliFlags & typCmdFunc) && menuTree[index].cliKey == 0) {
		sprintf(buffer, "%s", menuTree[index].cliString);
		menuTree[index].cb_cmd();
	} else if (menuTree[index].cliFlags & typVar) {
		if (menuTree[index].cliFlags & typInteger)
		{
			sprintf(buffer, menuTree[index].cliString, *((int *) menuTree[index].cb_var));
		} else {
			sprintf(buffer, menuTree[index].cliString,(char *) menuTree[index].cb_var);
		}
	} else {
		sprintf(buffer, "%s", menuTree[index].cliString);
	}

	return buffer;
}

/******************************************************
 * Function	   : kbhit
 * Input	   : None
 * Output	   : true = key available, false = no key
 * Description : Returns TRUE if a key was hit on the keyboard.
 ******************************************************/
bool kbhit()
{
	struct timeval tv;
	fd_set fds;

	if (global_argmode == 0)
	{
		tv.tv_sec = 0;
		tv.tv_usec = 1;
		FD_ZERO(&fds);
		FD_SET(STDIN_FILENO, &fds);
		select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
		return (FD_ISSET(0, &fds));
	} else {
		return 0;
	}
}
