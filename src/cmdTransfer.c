/*
 * cmdTransfer.c
 *
 *  Created on: Feb 20, 2017
 *      Author: bwilcutt
 */
#include <stdio.h>
#include <stdlib.h>
#include "cmdTransfer.h"

static int transmitImage(FILE *canFile);

/******************************************************
 * Function	   : cmdFirmware
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Writes firmware file to the device.
 ******************************************************/
int cmdFirmware(void)
{
	int retVal = CMD_ERROR;
	FILE *canFile = NULL;

	/* Open the outgoing image file */

	if ((canFile = fopen(var_firmwarefile, "rb")) == NULL)
	{
		cliPrint("CAN Image [%s] not found or not a file.\n", var_firmwarefile);
	} else {

		retVal = CMD_SUCCESS;
	}

	/* At this point, we have a file and a can socket to write to.  Begin. */

	if (retVal == CMD_SUCCESS)
	{
		retVal = transmitImage(canFile);
	}

	return retVal;
}

/******************************************************
 * Function	   : parseLine
 * Input	   : uint8_t * source data (IHEX ascii)
 * 				 int length of source buffer data
 * 				 uint8_t * dest data (binary)
 * 				 int maxmum dest buffer length
 * Output	   : Number of bytes parsed.
 * Description : This function parses an IHex Ascii line
 * 				 from a .hex file into binary.
 ******************************************************/
int parseLine(uint8_t *inBuffer, int inBufLen, uint8_t *outBuffer, int outBufLen)
{
	int retVal = 0; // Bytes copied
	int v;
	char buf[3];
	int x;
	uint8_t *o = outBuffer;
	uint8_t *i = inBuffer;

	if (inBuffer && outBuffer)
	{
		if (outBufLen >= ((inBufLen - 2) / 2))
		{
			if (*i++ == ':') // ":" marks a beginning line
			{
				// Read the remaining bytes, converting from ASCII to hex.

				for (x = 0; x < ((inBufLen - 3) / 2); x++) // -3 for ':', CR/LF delims.
				{
					buf[0] = *i++;
					buf[1] = *i++;
					buf[2] = 0;
					sscanf(buf, "%02x", &v);

					*o++ = (uint8_t) v;
					retVal++;
				}
			}
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : transmitImage
 * Input	   : FILE * of Ihex file to transmit
 * 				 sockaddr * of transmit socket.
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : This function transmits an Ihex file to the
 *   			 destination CAN socket.  The file is parsed
 *   			 from ascii hex to binary and formatted to
 *   			 follow the LynxSpring HEXDATA/HEXSTART
 *   			 protocol.
 *
 *   			 Caveat:  Data in an ihex file line should
 *   			 not exceed 64 bytes.
 ******************************************************/
static int transmitImage(FILE *canFile)
{
	int retVal = CMD_SUCCESS; // Error
	int lineSize;
	int bytesRemaining;
	int nxtCmd;
	int i;
	int bytesMoved = 0;
	long fsent = 0;
	long fsize = 0;
	uint8_t *p;
	char *lineBuffer = NULL;
	size_t bufSize = 0;
	uint8_t copyLen = 0;
	uint8_t bytesCopied = 0;
	uint8_t outData[255];
	uint8_t macbin[3];
	struct can_frame frame;
	lpp_id lpp;

	// Get size of file so we can print percentage of completion on the display

	fsize = getFileSize(var_firmwarefile);
	getMacAddr(macbin);

	cliPrint("\n\nStarting download, press <RETURN> to abort.\n");

	/* Send a CMD_IMAGE_START packet 3 times so we're sure it got out. */

	lpp.val = 0;
	lpp.bits.dtyp = TYPE_COMMAND;
	lpp.bits.mac0 = var_macint[0]; // Far end device to program
	lpp.bits.mac1 = var_macint[1];
	lpp.bits.mac2 = var_macint[2];
	lpp.bits.populate = HAS_ONE;
	lpp.bits.priority = 0;
	frame.can_id = lpp.val;
	frame.can_dlc = 5; // Id + Command + Respond MAC
	frame.data[0] = var_chipid;
	frame.data[1] = CMD_CODE_REQ;
	frame.data[2] = macbin[0];	// Who's your daddy?
	frame.data[3] = macbin[1];
	frame.data[4] = macbin[2];
	CAN_Transmit(&frame);

	for (i = 0; i < 3; i++)
	{
		CAN_Transmit(&frame);
	}

	sleep(1); // Time for device to erase flash

	/* Begin transmission of ihex formatted file.  One line at a time is transmitted. */

	while ((lineSize = getline(&lineBuffer, &bufSize, canFile)) > 0 && retVal == CMD_SUCCESS)
	{
		fsent += lineSize;
		cliPrint("Sending Data [%i%%]  \r", (int) (((float) ((float) fsent / (float) fsize) * 100)));

		if (kbhit())
			retVal = CMD_ERROR;

		// Read a whole single line from the Ihex file...

		if ((bytesCopied = parseLine((uint8_t *) lineBuffer, lineSize, outData, sizeof(outData))) != 0)
		{
			// Now transmit buffer to device 8 bytes at a time.

			bytesRemaining = bytesCopied;
			p = outData;

			do {
				if (bytesRemaining > IMAGE_PKT_SIZE)
				{
					// A beginning or continuation of a packet.

					copyLen = IMAGE_PKT_SIZE;
					nxtCmd = CMD_HEXDATA_IND;
				} else {
					// The last packet to send in the line

					copyLen = bytesRemaining;
					nxtCmd = CMD_HEXLAST_IND;
				}

				// Send the frame to the device

				bytesMoved += copyLen;

				frame.can_id = lpp.val;
				frame.can_dlc = 2 + copyLen; // Id + Command + Respond MAC
				frame.data[0] = var_chipid;
				frame.data[1] = nxtCmd;
				frame.data[2] = var_macint[0];
				frame.data[3] = var_macint[1];
				for (i = 0; i < copyLen; i++)
					frame.data[2 + i] = p[i];

				CAN_Transmit(&frame);

				if (bytesMoved == 64)
				{
					// Extra long pause to allow hard to write block to flash

					usleep(20 * 1000); // Delay 20ms between msgs
					bytesMoved = 0;
				}

				bytesRemaining -= copyLen;

				p += copyLen;

				// Delay to allow device possible flash write

				usleep(var_relaxTimer * 1000); // Delay 20ms
			} while (nxtCmd != CMD_HEXLAST_IND);
		}

		free(lineBuffer);
		bufSize = 0;
	}

	if (retVal == CMD_ERROR)
	{
		cliPrint("\nAborted..\n");
	}

	return retVal;
}


