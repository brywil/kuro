/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: exCmd.h
 *
 * About:
 *
 * Support info for exCmd.c.
 */

#ifndef EXCMD_H_
#define EXCMD_H_


#include "common.h"

#define SIZEOF_RESULT 2048

extern int executeCommand(char *cmdstr, char *outbuf);
extern char *strbstr(char *haystack, char stopchar, char *starting, char *needle);
extern void cliPrint(const char *fmt, ...);
extern bool terminalGetKey(char *, int) ;
extern int getInput(char *keybuf, int len, char *override_value);
extern void cliRestore();
extern void cliSetup();
extern int strnicmp(const char *s1, const char *s2, int len);
extern int stricmp (const char *s1, const char *s2);

extern char resultBuf[SIZEOF_RESULT];

#endif /* EXCMD_H_ */
