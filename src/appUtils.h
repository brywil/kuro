/*
 * appUtils.h
 *
 *  Created on: Feb 1, 2017
 *      Author: bwilcutt
 */

#ifndef SRC_APPUTILS_H_
#define SRC_APPUTILS_H_

#include "common.h"

extern void sendCANFrame(uint8_t *id, onyxx_cmd_t cmd_code, int chip_id, int len, uint8_t *data);
extern int readCANFrame(struct can_frame *frame);
extern int openCAN();
extern void getMacAddr(uint8_t *macint);
extern long getFileSize(char *fname);
extern void CAN_Transmit(struct can_frame *frame);

#endif /* SRC_APPUTILS_H_ */
