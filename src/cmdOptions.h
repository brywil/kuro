/*
 * Program		: Kuro
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: cmdOptions.h
 *
 * About:
 *
 * Support API functions for cmdOptions.c.
 */

#ifndef CMDOPTIONS_H_
#define CMDOPTIONS_H_


#include "common.h"
#include "exCmd.h"

extern int cmdProductInfo(void);
extern int cmdShowDevices(void);
extern int cmdProgramMAC(void);
extern int cmdShowMAC(void);
extern int cmdGetAppID(void);
extern int cmdGetConfInfo(void);

#endif /* CMDOPTIONS_H_ */
