This repository contains the KURO (Japanese for "Black" or "Onyxx") application.  This program runs on a Linux platform.

Build environment:

1. Build with gcc/ln x86_64-linux-gnu 4.8.5
2. Eclipse (MARS although others should work)

This application provides not only a working platform but a test bench for the Lynxspring Peripheral Protocol (versioned as of this date).  Functionality provided includes:

1. Set the local CAN device name
2. Set the firmware file name
3. Set the local MAC address
4. Set the Relax timer
5. Program Firmware
6. Program MAC
7. Ping for CAN Devices
8. Display local Linux PC CAN Devices
9. Get Running App ID from application (once booted)
10. Get Device Configuration
11. Set Device Configuration

**Local CAN**

This is usually set to "can0" but can be set to whichever other CAN device is identified.  This will be the device used to communicate to 534/XMB modules.

**Firmware File**

This is the firmware file chosen to push to a PIC chip.  The decision on which chip the file will be pushed is based on the file name given here.  Use "a0", "d0", "ui_l", "ui_h", "led" or "app" in the file name.  A name is not necessary to specify if no firmware is to be pushed.

**Local MAC Address**

This is a 3-byte ASCII HEX string that tells Kuro the destination's MAC address.  Use the format "xx:xx:xx" (ex: 5a:2e:11).  When communicating to a device, this will be the MAC address specified in the LPP protocol.

**Relax Timer**

This is a "kludge" factor timer.  If on a noisy CAN bus, increase this timer to instruct Kuro to avoid jamming the CAN bus further.  A typical value is "7".

**Program Firmware**

Programming firmware requires the MAC address and firmware file name to be specified.  Firmware is pushed using the Lynxspring Peripheral Protocol.

**Program MAC**

If setting a MAC for the first time, use this function.  Programming a MAC is only allowable while the system is in the bootloader-- applications do not support this feature. You may need to first PING the device to force it into "monitor mode".  Monitor mode stops the boot timer (2-3 secs) from booting any available application in memory.

**Ping for Devices**

Use this function to request all CAN devices supporting the Lynxspring Peripheral Protocol to report their MAC addresses. The received address and associated Chip type will be displayed.  Note that both the bootloader and the application support this feature.  If you are trying to put the device into "monitor mode", make sure you Ping within the 2-3 sec window (while still in bootloader).  Once in monitor mode, the system will not automatically boot the application unless a firmware is pushed or the device has been power cycled.

**Display Local CAN Devices**

Displays devices on the local PC that are marked as CAN devices.

**Running Application ID**

This function causes all running applications (note, bootloader is NOT an application) to report a 6-byte HEX value that should correspond to the running application's version number.  The 6-byte field is purely arbitrary as there is no format specified.  You can use this function to see if the currently running application is of an expected version.

**Get and Set Device Configuration**

These are purely test functions for getting and setting a gratuitous configuration structure.  Both of these functions were built just to test the Get/Set File operation of the Lynxspring Peripheral Protocol.  The intended use is to allow engineers to see how to use the protocol through a real example-- see source code.


Bryan Wilcutt
4.4.2017